
fun main() {
    val str1 = """
        5 100
        x1 30
        x2 170
        x3 120
        x4 350
        x5 230
    """.trimIndent()

    println(alignResult(calculateEachElapsedTime(str1)))
    println("")
    println(alignResult(calculateEachElapsedTimeMathematically(str1)))
}



/**
 * calculateEachElapsedTimeの返した結果を
 * 整形して文字列にするメソッド
 */
fun alignResult(resultList: MutableList<Pair<String, Int>>) : String {
    return resultList
        .map{ it.first + " " + it.second.toString() }
        .joinToString(separator = "\n")
}


/**
 * Queueプロセス毎に, プロセス終了時の経過時間を返すメソッド
 */
fun calculateEachElapsedTime(str: String): MutableList<Pair<String, Int>> {
    val answerList = mutableListOf<Pair<String, Int>>()
    val queueProps = MakeQueueList(str)

    var routine = 0 // while繰り返し回数
    var elapsedTime = 0 // 経過時間をストック
    /**
     * プロセスが全て終了するまで、キューを回すループ
     */
    while (queueProps.queueList.size > 0) {


        // 終わったプロセスを記録する除外リスト. 後でまとめてキューリストから除外。
        val excludeList = mutableListOf<Int>()
        /**
         * キューの一周分のループ
         */
        queueProps.queueList.forEachIndexed { index, eachProcess ->
            // プロセス残り時間とクオンタムの差分の正負でプロセス終了を判別
            val diff = (eachProcess[1] as Int) - queueProps.quantum
            if (diff > 0) {
                elapsedTime += queueProps.quantum //経過時間を追加
                queueProps.queueList[index][1] = diff //残り所要時間からクオンタムを引く
            } else {
                //経過時間を追加
                elapsedTime += queueProps.quantum + diff
                //返り値に加える
                answerList.add(
                    Pair(eachProcess[0] as String, elapsedTime)
                )
                //除外リストに加える
                excludeList.add(index)
            }
        }

        //除外リストに加わったプロセスをキューリストから除外する。
        val deletedQueueList = queueProps.queueList.filterIndexed{ index, mutableList ->
            index !in excludeList
        }.toMutableList()
        queueProps.queueList = deletedQueueList

        routine ++
    }

    return answerList
}

/**
 * 数学的に求めてみた方
 *
 */
fun calculateEachElapsedTimeMathematically(str: String): MutableList<Pair<String, Int>> {
    val answerList = mutableListOf<Pair<String, Int>>()
    val queueProps = MakeQueueList(str)

    /**
     * プロセス終了にかかる処理回数を記録するリスト
     */
    val proccessingNumberList = queueProps.queueList.map{
        val proccessingNumber = (it[1] as Int) / queueProps.quantum
        if ((it[1] as Int) % queueProps.quantum == 0) proccessingNumber
        else proccessingNumber + 1
    }

    /**
     * 数学的に算出した経過時間をもとに、全てのプロセスの経過時間を算出する
     */
    queueProps.queueList.forEachIndexed { index, mutableList ->
        /**
         * 全てのプロセスが終わらないと仮定したときの、そのプロセスの実際の終了回数までの処理時間
         */
        val wholeTime = ((proccessingNumberList[index] - 1) * queueProps.number + (index + 1)) * queueProps.quantum

        /**
         * 終わっていたはずプロセスがwholeTimeに足しすぎた差分をそのプロセス以前と後に分けて計算する。
         */
        val thisProccessingNumber = proccessingNumberList[index]
        // そのプロセス以前の順番のプロセスがそのプロセスよりも前に終了していた場合の差分
        var beforeDiff = 0
        for (i in 0..index) {
            if (thisProccessingNumber >= proccessingNumberList[i]) {
                beforeDiff += queueProps.quantum * thisProccessingNumber - (queueProps.queueList[i][1] as Int)
            }
        }
        // そのプロセスより後の順番のプロセスがそのプロセスよりも前に終了していた場合の差分
        var afterDiff = 0
        for (i in index + 1 until queueProps.queueList.size) {
            if (thisProccessingNumber - 1 >= proccessingNumberList[i]) {
                afterDiff += queueProps.quantum * (thisProccessingNumber-1) - (queueProps.queueList[i][1] as Int)
            }
        }

        /**
         * 足しすぎた差分を引くと答えになる。
         */
        val elapsedTime = wholeTime - beforeDiff - afterDiff
        answerList.add(Pair(queueProps.queueList[index][0] as String, elapsedTime))
    }

    // 経過時間順に答えを並べ替える
    answerList.sortBy { it.second }

    return answerList
}

/**
 * Queueの要素を生の入力文字列から生成σルウクラス
 *
 * @param
 * 生のQueue文字列
 */
class MakeQueueList(queueStr: String) {
    val number: Int
    val quantum: Int
    var queueList: MutableList<MutableList<Any>>

    /**
     * @param
     * number,quantum,queueList
     * @throws
     * 範囲外のnumber, quantum
     */
    init {
        val result = initializeProperties(queueStr)
        // TODO 3つのプロパティを同時に設定したいが、わからないので保留
        number = result.component1()
        quantum = result.component2()
        queueList = result.component3()
        require(number in 1..100000) {"number of process is outOfRange"}
        require(quantum in 1..1000) {"quantum is outOfRange"}
    }

    /**
     * 初期化メソッド。
     *
     * @params
     * 生の文字列入力
     * @return
     * プロセス数、クオンタム、1プロセスごとに要素に分かれたリスト
     */
    private fun initializeProperties(queueStr: String) : Triple<Int, Int, MutableList<MutableList<Any>>>{
        val _rawQueueList = queueStr.split("\n").toMutableList()
        val _rawQueueList2 = _rawQueueList.map {
            it.split(" ")
        }.toMutableList()
        val index0 = _rawQueueList2.removeAt(0)
        val rawQueueList = _rawQueueList2.map{ mutableListOf(it[0], it[1].toInt()) }.toMutableList()
        return Triple(index0[0].toInt(), index0[1].toInt(), rawQueueList)
    }
}






